/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebTests;

import Pages.TinkoffCityChangePopup;
import Pages.TinkoffCommonPage;
import Pages.TinkoffCommunalPaymentsPage;
import Pages.TinkoffStartPage;
import Pages.TinkoffPaymentsPage;
import Pages.TinkoffZhkuMoskvaPage;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author Strokoff Mikhail
 */
public class JUnitTest {

    private RemoteWebDriver webDriver;
    private TinkoffStartPage startPage;
    private TinkoffPaymentsPage paymentsPage;
    private TinkoffCommunalPaymentsPage communalPaymentsPage;
    private TinkoffZhkuMoskvaPage zhkuMoskvaPage;
    private TinkoffCityChangePopup cityChangePopup;

    public JUnitTest() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/ChromeDriver/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        webDriver.get("https://www.tinkoff.ru");
    }

    @After
    public void tearDown() {
        webDriver.quit();
    }

    @Test
    public void CommunalPaymentsValidationTest() {
        /* Шаг 1 */
        startPage = new TinkoffStartPage(webDriver);

        /* Шаг 2 */
        startPage.PaymentsLink.click();
        paymentsPage = new TinkoffPaymentsPage(webDriver);

        /* Шаг 3 */
        paymentsPage.CommunalPaymentsLink.click();
        communalPaymentsPage = new TinkoffCommunalPaymentsPage(webDriver);

        /* Шаг 4 */
        if (!communalPaymentsPage.CheckCurrentRegion("Москве")) {
            communalPaymentsPage.OpenChangeCityPopup();
            cityChangePopup = new TinkoffCityChangePopup(webDriver);
            cityChangePopup.SelectMoscow();
            communalPaymentsPage = new TinkoffCommunalPaymentsPage(webDriver);
        }

        /* Шаг 5 */
        String paymentName = communalPaymentsPage.SelectCommunalPayment(0);
        zhkuMoskvaPage = new TinkoffZhkuMoskvaPage(webDriver);
        String zhkuMoskvaUrl = webDriver.getCurrentUrl();

        /* Шаг 6 */
        zhkuMoskvaPage.ZhkuMoskvaPayment.click();
        zhkuMoskvaPage = new TinkoffZhkuMoskvaPage(webDriver);

        /* Шаг 7 */
        String requiredFieldValidationMessage = "Поле обязательное";
        String payerCodeValidationMessage = "Поле неправильно заполнено";
        String dateValidationMessage = "Поле заполнено некорректно";
        String minSummValidationMessage = "Минимальная сумма перевода - 10 ₽";
        String maxSummValidationMessage = "Максимальная сумма перевода - 15 000 ₽";

        zhkuMoskvaPage.CheckRequiredFieldsValidationMessage(requiredFieldValidationMessage);

        zhkuMoskvaPage.CheckPayerCodeValidationMessage("123456789", payerCodeValidationMessage);
        zhkuMoskvaPage.CheckPayerCodeValidationMessage("12345678901", payerCodeValidationMessage);
        zhkuMoskvaPage.CheckPayerCodeValidationMessage("123456789f", payerCodeValidationMessage);
        zhkuMoskvaPage.CheckPayerCodeValidationMessage("1234567890", "");

        zhkuMoskvaPage.CheckDateValidationMessage("1.1999", dateValidationMessage);
        zhkuMoskvaPage.CheckDateValidationMessage("13.1999", dateValidationMessage);
        zhkuMoskvaPage.CheckDateValidationMessage("00.1999", dateValidationMessage);
        zhkuMoskvaPage.CheckDateValidationMessage("01.1999", "");
        zhkuMoskvaPage.CheckDateValidationMessage("12.1999", "");

        zhkuMoskvaPage.CheckSummValidationMessage("9", minSummValidationMessage);
        zhkuMoskvaPage.CheckSummValidationMessage("10", "");
        zhkuMoskvaPage.CheckSummValidationMessage("15001", maxSummValidationMessage);
        zhkuMoskvaPage.CheckSummValidationMessage("15000", "");

        /* Шаг 8 */
        webDriver.navigate().refresh();
        zhkuMoskvaPage = new TinkoffZhkuMoskvaPage(webDriver);
        zhkuMoskvaPage.PaymentsLink.click();
        paymentsPage = new TinkoffPaymentsPage(webDriver);

        /* Шаг 9-11 */
        paymentsPage.CheckFirstSearchResult(paymentName)
                .click();

        zhkuMoskvaPage = new TinkoffZhkuMoskvaPage(webDriver);
        assertEquals("Unexpected URL !",
                zhkuMoskvaUrl,
                webDriver.getCurrentUrl());

        /* Шаг 12 */
        zhkuMoskvaPage.PaymentsLink.click();
        new TinkoffPaymentsPage(webDriver).CommunalPaymentsLink.click();

        /* Шаг 13 */
        communalPaymentsPage = new TinkoffCommunalPaymentsPage(webDriver);
        if (!communalPaymentsPage.CheckCurrentRegion("Санкт-Петербурге")) {
            communalPaymentsPage.OpenChangeCityPopup();
            cityChangePopup = new TinkoffCityChangePopup(webDriver);
            cityChangePopup.SelectRegion("Санкт-Петербург");
            communalPaymentsPage = new TinkoffCommunalPaymentsPage(webDriver);
        }

        /* Шаг 14 */
        String assertMessage = "Founded '"
                .concat(paymentName)
                .concat("', but not expected!");
        assertNull(assertMessage,
                communalPaymentsPage.GetCommunalPayment(paymentName));
    }
}
