/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.TinkoffElement;
import Elements.TinkoffElementsList;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Страница платежей
 *
 * @author Strokoff Mikhail
 */
public class TinkoffPaymentsPage extends TinkoffCommonPage {

    public final String Url = "https://www.tinkoff.ru/payments/";

    /* Header Menu */
    public WebElement CommunalPaymentsLink;

    public WebElement PaymentSearchInput;

    private WebDriver driver;

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffPaymentsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        CommunalPaymentsLink = new TinkoffElement(driver, By.xpath("//*[@title='Коммунальные платежи']"))
                .WaitReadyElement();
        PaymentSearchInput = new TinkoffElement(driver, By.xpath("//*[@class='_3kceY']//input"))
                .WaitReadyElement();
    }

    /**
     * Выполняет поиск по получателям платежа.
     *
     * @param searchString - строка поиска
     * @return - список найденных получателей платежа
     */
    public List<WebElement> GetSearchResults(String searchString) {
        PaymentSearchInput.sendKeys(searchString);
        Wait(3000);
        return new TinkoffElementsList(driver, By.className("_2vlxq"))
                .WaitReadyElements(false);
    }

    /**
     * Выполняет поиск по получателям платежа и возвращает первого из списка
     * найденных.
     *
     * @param searchString - строка поиска
     * @return - первый из найденных получателей платежа
     */
    public WebElement CheckFirstSearchResult(String searchString) {
        WebElement result = GetSearchResults(searchString).get(0);

        assertTrue("Unexpected first search result !",
                result.getText().contains(searchString));

        return result;
    }
}
