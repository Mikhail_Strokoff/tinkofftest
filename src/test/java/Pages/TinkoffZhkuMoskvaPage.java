/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.NoSuchElementException;
import static org.junit.Assert.assertEquals;

/**
 * Страница Жку-Москва
 *
 * @author Admin
 */
public class TinkoffZhkuMoskvaPage extends TinkoffCommonPage {

    public String Url = "https://www.tinkoff.ru/zhku-moskva/";

    public WebElement ZhkuMoskvaPayment;
    private final By inputsBy = By.xpath("//*[@class='ui-form__field']//input");
    private final By validationMessageXpath = By.xpath("//*[@class='ui-form__field']");
    private final By validationMessageClass = By.className("ui-form-field-error-message");
    private final int payerCodeIndex = 0;
    private final int dateIndex = 1;
    private final int summIndex = 3;

    private final RemoteWebDriver webDriver;

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffZhkuMoskvaPage(RemoteWebDriver driver) {
        super(driver);
        webDriver = driver;
        ZhkuMoskvaPayment = driver.findElement(By.xpath("//a[@title='Оплатить ЖКУ в Москве']"));
    }

    /**
     * Вводит код плательщика
     *
     * @param payerCode - код плательщика
     */
    public void SetPayerCode(String payerCode) {
        SetField(payerCode, payerCodeIndex);
    }

    /**
     * Вводит период оплаты
     *
     * @param date - период оплаты
     */
    public void SetDate(String date) {
        SetField(date, dateIndex);
    }

    /**
     * Вводит сумму платежа
     *
     * @param summ - сумма платежа
     */
    public void SetSumm(String summ) {
        SetField(summ, summIndex);
    }

    private void SetField(String value, int index) {
        WebElement input = webDriver.findElements(inputsBy).get(index);
        input.clear();
        if (!value.isEmpty()) {
            input.sendKeys(value);
        }
        ZhkuMoskvaPayment.sendKeys("");

        Wait(1500);
    }

    /**
     * Вводит код плательщика и проверяет валидационное сообщение
     *
     * @param payerCode - код плательщика
     * @param expectedMessage - ожидаемое сообщение
     */
    public void CheckPayerCodeValidationMessage(String payerCode, String expectedMessage) {
        SetPayerCode(payerCode);
        CheckValidationMessage(expectedMessage, payerCodeIndex);
    }

    /**
     * Вводит период оплаты и проверяет валидационное сообщение
     *
     * @param date - период оплаты
     * @param expectedMessage - ожидаемое сообщение
     */
    public void CheckDateValidationMessage(String date, String expectedMessage) {
        SetDate(date);
        CheckValidationMessage(expectedMessage, dateIndex);
    }

    /**
     * Вводит сумму оплаты и проверяет валидационное сообщение
     *
     * @param summ - сумма оплаты
     * @param expectedMessage - ожидаемое сообщение
     */
    public void CheckSummValidationMessage(String summ, String expectedMessage) {
        SetSumm(summ);
        CheckValidationMessage(expectedMessage, summIndex);
    }

    private void CheckValidationMessage(String expectedMessage, int index) {
        WebElement validationMessage;
        try {
            validationMessage = webDriver
                    .findElements(validationMessageXpath).get(index)
                    .findElement(validationMessageClass);
        } catch (NoSuchElementException e) {
            if (expectedMessage.isEmpty()) {
                return;
            } else {
                throw e;
            }
        }
        assertEquals(
                "Unexpected payer's code validation message! ",
                expectedMessage,
                validationMessage.getText());
    }

    /**
     * Проверка валидационных сообщений для обязательных полей.
     *
     * @param expectedMessage - ожидаемое сообщение
     */
    public void CheckRequiredFieldsValidationMessage(String expectedMessage) {
        webDriver.findElement(By.className("ui-button_provider-pay")).click();
        CheckValidationMessage(expectedMessage, payerCodeIndex);
        CheckValidationMessage(expectedMessage, dateIndex);
        CheckValidationMessage(expectedMessage, summIndex);
    }
}
