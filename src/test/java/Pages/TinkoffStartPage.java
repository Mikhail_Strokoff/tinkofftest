/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.TinkoffElement;
import Elements.TinkoffElementsList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Стартовая страница
 *
 * @author Strokoff Mikhail
 */
public class TinkoffStartPage extends TinkoffCommonPage {

    public final String Url = "https://www.tinkoff.ru/";

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffStartPage(WebDriver driver) {
        super(driver);
    }
}
