/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.TinkoffElementsList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Класс-предок всех страниц
 *
 * @author Admin
 */
public class TinkoffCommonPage {

    /* Header Menu */
    public WebElement PaymentsLink;

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffCommonPage(WebDriver driver) {
        //на стартовой странице меню задваивается, из-за этого находится не тот элемент.
        List<WebElement> prelist = new TinkoffElementsList(driver, By.xpath("//*[@href='/payments/']"))
                .WaitReadyElements();
        PaymentsLink = prelist.get(prelist.size() - 1);
    }

    /**
     * Безусловное ожидание в течение заданного времени.
     * 
     * @param l - время в миллисекундах
     */
    public void Wait(long l){
        try {
            Thread.sleep(l);
        } catch (InterruptedException ex) {
            Logger.getLogger(TinkoffCommonPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
