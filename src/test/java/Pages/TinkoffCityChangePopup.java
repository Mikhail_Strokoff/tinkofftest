/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.TinkoffElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Попап выбора города
 *
 * @author Admin
 */
public class TinkoffCityChangePopup {

    public WebElement CityChangeInput;

    public WebDriver driver;

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffCityChangePopup(WebDriver driver) {
        this.driver = driver;
        CityChangeInput = new TinkoffElement(driver, By.className("ui-input__field"))
                .WaitReadyElement();
    }

    /**
     * Выбор региона "Москва".
     */
    public void SelectMoscow() {
        SelectRegion("Москва");
    }

    /**
     * Выбирает регион из списка. Важно: текущий попап закроется.
     *
     * @param region - название региона
     */
    public void SelectRegion(String region) {
        CityChangeInput.sendKeys(region);

        String xpath = "//span[contains(text(), '"
                .concat(region)
                .concat("')]");

        new TinkoffElement(driver, By.xpath(xpath)).WaitReadyElement().click();
    }
}
