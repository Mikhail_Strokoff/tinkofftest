/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import Elements.TinkoffElement;
import Elements.TinkoffElementsList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Страница коммунальных платежей
 *
 * @author Strokoff Mikhail
 */
public class TinkoffCommunalPaymentsPage extends TinkoffCommonPage {

    public final String Url = "https://www.tinkoff.ru/payments/categories/kommunalnie-platezhi/";

    private final TinkoffElement CurrentCityElement;

    public WebElement CurrentCity;
    public List<WebElement> communalPayments;

    /**
     *
     * @param driver - используемый экземпляр WebDriver'а
     */
    public TinkoffCommunalPaymentsPage(WebDriver driver) {
        super(driver);
        CurrentCityElement = new TinkoffElement(driver, By.className("payment-page__title_inner"));
        //CurrentCity = CurrentCityElement.WaitReadyElement();
        communalPayments = new TinkoffElementsList(driver, By.xpath("//ul[@class = 'ui-menu ui-menu_icons _1-BXo']/li"))
                .WaitReadyElements();
    }

    /**
     * Открывает попап выбора региона.
     */
    public void OpenChangeCityPopup() {
        CurrentCityElement.WaitReadyElement().click();
    }

    /**
     * Сравнивает текущий регион с ожидаемым. Важно: название региона
     * указывается в предложном ("Где?") падаже.
     *
     * @param region - ожидаемый регион
     * @return - Возвращает true, если текущий регион совпадает с ожидаемым
     */
    public boolean CheckCurrentRegion(String region) {
        return CurrentCityElement.WaitReadyElement().getAttribute("innerText").equals(region);
    }

    /**
     * Выбирает поставщика услуг ЖКХ по порядковому номеру. Важно: отсчет
     * начинается с 0.
     *
     * @param number - порядковый номер поставщика услуг.
     * @return - возвращает название поставщика услуг
     */
    public String SelectCommunalPayment(int number) {
        String result = communalPayments.get(number).getText();
        communalPayments.get(number).click();
        return result;
    }
    
    /**
     * Возвращает ссылку на поставщика услуг ЖКХ по названию.
     *
     * @param name - название поставщика услуг.
     * @return - элемент со ссылкой на поставщика услуг
     */
    public WebElement GetCommunalPayment(String name) {
        for (WebElement elem : communalPayments){
            if (elem.getText().equals(name))
                return elem;
        }
        return null;
    }

    /**
     * Выбирает поставщика услуг ЖКХ по названию.
     *
     * @param name - название поставщика услуг.
     */
    public void SelectCommunalPayment(String name) {
        GetCommunalPayment(name).click();
    }
}
