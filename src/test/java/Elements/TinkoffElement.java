/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Слой работы в WebElement с отложенным вызовом
 *
 * @author Admin
 */
public class TinkoffElement {

    private final By by;
    private final WebDriver driver;

    public TinkoffElement(WebDriver driver, By by) {
        this.by = by;
        this.driver = driver;
    }

    /**
     * Получение елемента по настроенному через конструктор селектору за
     * отведённое время.
     *
     * @param timeout - время, за которое элемент будет найден
     * @return - найденный элемент
     */
    public WebElement WaitReadyElement(int timeout) {
        int timeoutPart = timeout / 5;
        String message = "Element '"
                .concat(String.valueOf(by))
                .concat(String.valueOf("' not visible at "))
                .concat(String.valueOf(timeout));

        for (int i = 0; i <= 5; i++) {
            try {
                if (i > 0) {
                    Thread.sleep(timeoutPart); // первая итерация без ожидания
                }
                WebElement element = driver.findElement(by);
                if (element.isDisplayed()) {
                    return element;
                }
            } catch (InterruptedException ex) {
                throw new ElementNotVisibleException(message);
            } catch (StaleElementReferenceException | NoSuchElementException exception) {

            }
        }

        throw new ElementNotVisibleException(message);
    }

    /**
     * Получение елемента по настроенному через конструктор селектору за
     * определённое время (15 сек).
     *
     * @return - найденный элемент
     */
    public WebElement WaitReadyElement() {
        return WaitReadyElement(15000);
    }
}
