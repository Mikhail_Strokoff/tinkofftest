/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Elements;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Список элементов с отложенным вызовом
 *
 * @author Admin
 */
public class TinkoffElementsList {

    private final By by;
    private final WebDriver driver;

    public TinkoffElementsList(WebDriver driver, By by) {
        this.by = by;
        this.driver = driver;
    }

    /**
     * Получение списка елементов по настроенному через конструктор селектору за
     * отведённое время.
     *
     * @param canBeNull - может ли список быть пустым
     * @param timeout - время, за которое элементы будут найдены
     * @return - список найденных элементов
     */
    public List<WebElement> WaitReadyElements(boolean canBeNull, int timeout) {
        int timeoutPart = timeout / 5;
        String message = "Element '"
                .concat(String.valueOf(by))
                .concat(String.valueOf("' not visible at "))
                .concat(String.valueOf(timeout));

        for (int i = 0; i <= 5; i++) {
            try {
                if (i > 0) {
                    Thread.sleep(timeoutPart); // первая итерация без ожидания
                }
                List<WebElement> elements = driver.findElements(by);
                if (!elements.isEmpty()) {
                    return elements;
                }
            } catch (InterruptedException ex) {
                throw new ElementNotVisibleException(message);
            } catch (StaleElementReferenceException exception) {

            }
        }

        //иногда список может быть ожидаемо пуст - например, при поиске
        if (canBeNull) {
            return new ArrayList<>();
        }

        throw new ElementNotVisibleException(message);
    }

    /**
     * Получение списка елементов по настроенному через конструктор селектору за
     * определённое время (15 сек).
     *
     * @return - список найденных элементов
     */
    public List<WebElement> WaitReadyElements() {
        return WaitReadyElements(true, 15000);
    }

    /**
     * Получение списка елементов по настроенному через конструктор селектору за
     * определённое время (15 сек).
     *
     * @param canBeNull - может ли список быть пустым
     * @return - список найденных элементов
     */
    public List<WebElement> WaitReadyElements(boolean canBeNull) {
        return WaitReadyElements(canBeNull, 15000);
    }
}
